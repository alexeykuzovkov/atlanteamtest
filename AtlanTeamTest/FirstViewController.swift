//
//  FirstViewController.swift
//  AtlanTeamTest
//
//  Created by Alex Kuzovkov on 05.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct Segues {
    static let postViewSegue        = "postViewSegue"
    static let commentsViewSegue    = "commentsViewSegue"
    static let usersViewSegue       = "usersViewSegue"
    static let photoViewSegue       = "photoViewSegue"
    static let todosViewSegue       = "todosViewSegue"
}

struct Endpoints {
    static let posts        = "https://jsonplaceholder.typicode.com/posts"
    static let comments     = "https://jsonplaceholder.typicode.com/comments"
    static let users        = "https://jsonplaceholder.typicode.com/users"
    static let photos       = "https://jsonplaceholder.typicode.com/photos"
    static let todos        = "https://jsonplaceholder.typicode.com/todos"
}
class FirstViewController: UITableViewController, PostViewControllerDelegate, CommentViewControllerDelegate {
    
    var usersViewController:UsersViewController = UsersViewController()
    var photoViewController:PhotoViewController = PhotoViewController()
    var todosViewController:ToDosViewController = ToDosViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadUsers()
        loadPhoto()
        loadTodos()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Load Users
    func loadUsers() {
        usersViewController.usersCount = 5
        usersViewController.showUserViewControllers()
        
        for (index,userVC) in usersViewController.usersViewControllers.enumerated() {
            userVC.acitivityIndicator.startAnimating()
            loadJSONFrom(endpoint: Endpoints.users+"/\(index+1)", callback: { (json, error) in
                if let json = json {
                    userVC.acitivityIndicator.stopAnimating()
                    userVC.nameLabel.text = json["name"].stringValue
                    userVC.nicknameLabel.text = json["username"].stringValue
                    userVC.emailLabel.text = json["email"].stringValue
                    
                    let jAdress = json["address"]
                    let street = jAdress["street"].stringValue
                    let suite = jAdress["suite"].stringValue
                    let city = jAdress["city"].stringValue
                    let zipcode = jAdress["zipcode"].stringValue
                    
                    userVC.adressLabel.text = "\(zipcode), \(city), \(street), \(suite)"
                    
                    userVC.phoneLabel.text = json["phone"].stringValue
                    userVC.websiteLabel.text = json["website"].stringValue
                    
                    userVC.companyNameLabel.text = json["company"]["name"].stringValue
                    userVC.companyCatchPhraseLabel.text = json["company"]["catchPhrase"].stringValue
                    userVC.companyBSLabel.text = json["company"]["bs"].stringValue
                }
                else {
                    print(error!.localizedDescription)
                }
            })
        }
    }
    //MARK: Load photo
    func loadPhoto() {
        photoViewController.activityIndicator.startAnimating()
        
        loadJSONFrom(endpoint: Endpoints.photos+"/3") { (json, error) in
            if let json = json {
                Alamofire.request(URL.init(string: json["url"].stringValue)!).responseData { (response) in
                    if response.error == nil {
                        if let data = response.data {
                            self.photoViewController.imageView.image = UIImage(data: data)
                        }
                    }
                    self.photoViewController.activityIndicator.stopAnimating()
                 }
            }
            else {
                self.photoViewController.activityIndicator.stopAnimating()
                print(error!.localizedDescription)
            }
        }
        
    }
    
    //MARK: Load todos 
    
    func loadTodos() {
        todosViewController.activityIndicator.startAnimating()
        todosViewController.hideElements()
        loadJSONFrom(endpoint: Endpoints.todos+"/\(Int(arc4random_uniform(200))+1)") { (json, error) in
            if let json = json {
                self.todosViewController.activityIndicator.stopAnimating()
                self.todosViewController.titleLabel.text = json["title"].stringValue
                self.todosViewController.completionSwitch.isHidden = false
                self.todosViewController.completionSwitch.isOn = json["completed"].boolValue
            }
            else {
                self.photoViewController.activityIndicator.stopAnimating()
                print(error!.localizedDescription)
            }
        }

    }
    
    //MARK: PostViewControllerDelegate
    
    func postViewControllerRequestsPost(viewController: PostViewController, postID: Int) {
        
        viewController.loadActivityIndicator.startAnimating()
        viewController.clearLabels()
        viewController.enterPostNumberMessageLabel.isHidden = true
        
        let postEndpoint = Endpoints.posts + "/\(postID)"
        loadJSONFrom(endpoint: postEndpoint) { (json, error) in
            if let json = json {
                viewController.loadActivityIndicator.stopAnimating()
                viewController.titleLabel.text = json["title"].stringValue
                viewController.bodyLabel.text = json["body"].stringValue
            }
            else {
                print(error!.localizedDescription)
            }
        }
    }
    
    //MARK: CommentViewControllerDelegate
    
    func commentViewControllerRequestsComment(viewController: CommentViewController, commentID: Int) {
        viewController.loadActivityIndicator.startAnimating()
        viewController.clearLabels()
        viewController.enterCommentNumberMessageLabel.isHidden = true
        
        let commentEndpoint = Endpoints.comments + "/\(commentID)"
        loadJSONFrom(endpoint: commentEndpoint) { (json, error) in
            if let json = json {
                viewController.loadActivityIndicator.stopAnimating()
                viewController.nameLabel.text = json["name"].stringValue
                viewController.emailLabel.text = json["email"].stringValue
                viewController.bodyLabel.text = json["body"].stringValue
            }
            else {
                print(error!.localizedDescription)
            }
        }

    }
    
    //MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier==Segues.postViewSegue {
            let postViewController = segue.destination as! PostViewController
            postViewController.maxPostID = 100
            postViewController.delegate = self
        }
        if segue.identifier==Segues.commentsViewSegue {
            let commentViewController = segue.destination as! CommentViewController
            commentViewController.maxCommentID = 500
            commentViewController.delegate = self
        }
        if segue.identifier==Segues.usersViewSegue {
            usersViewController = segue.destination as! UsersViewController
        }
        
        if segue.identifier==Segues.photoViewSegue {
            photoViewController = segue.destination as! PhotoViewController
        }
        
        if segue.identifier==Segues.todosViewSegue {
            todosViewController = segue.destination as! ToDosViewController
        }
        
    }
    
    //MARK: API Loader
    
    func loadJSONFrom(endpoint: String, callback:@escaping ((_ json: JSON?, _ error: Error?) -> Void)) {
        Alamofire.request(endpoint)
            .responseJSON { response in
                guard response.result.error == nil else {
                    callback(nil, response.result.error)
                    return
                }
                
                guard let json = response.result.value as? [String: Any] else {
                    callback(nil, response.result.error)
                    return
                }
                
                callback(JSON(json), nil)
        }
    }
}


