//
//  GradientView.swift
//  FitnessMessage
//
//  Created by Alex Kuzovkov on 29.08.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable
class ShadowView: UIView {
    @IBInspectable var shadowColor:    UIColor =  .black {didSet { updateShadow() }}
    @IBInspectable var shadowOpacity:   Float =  0.0 {didSet { updateShadow() }}
    @IBInspectable var shadowRadius:   CGFloat =  0.0 {didSet { updateShadow() }}
    @IBInspectable var shadowOffset:    CGSize =  CGSize.init(width: 0, height: 0) {didSet { updateShadow() }}
    
    func updateShadow() {
        self.layer.shadowColor = shadowColor.cgColor;
        self.layer.shadowOffset = shadowOffset;
        self.layer.shadowOpacity = shadowOpacity;
        self.layer.shadowRadius = shadowRadius;
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateShadow()
    }
}
