//
//  ToDosViewController.swift
//  AtlanTeamTest
//
//  Created by Alex Kuzovkov on 06.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit

class ToDosViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var completionSwitch: UISwitch!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        completionSwitch.isHidden = true
        titleLabel.text = ""

        // Do any additional setup after loading the view.
    }
    
    func hideElements() {
        completionSwitch.isHidden = true
        titleLabel.text = ""
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
