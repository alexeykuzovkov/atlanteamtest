//
//  UsersViewController.swift
//  AtlanTeamTest
//
//  Created by Alex Kuzovkov on 06.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {

    @IBOutlet var cardView: ShadowView!
    
    var usersViewControllers = [UserViewController]()
    
    var usersCount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func showUserViewControllers() {
        usersViewControllers = []
        var topAnchor:NSLayoutYAxisAnchor = cardView.topAnchor
        for _ in 1...usersCount {
            let containerView = UIView()
            containerView.translatesAutoresizingMaskIntoConstraints = false
            cardView.addSubview(containerView)
            
            let heightConstraint = NSLayoutConstraint(item: containerView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: cardView.frame.size.height/CGFloat(usersCount))
            
            NSLayoutConstraint.activate([
                containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
                containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
                containerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
                heightConstraint
                ])
            
            topAnchor = containerView.bottomAnchor
            
            let controller = storyboard!.instantiateViewController(withIdentifier: "UserViewController") as! UserViewController
            addChildViewController(controller)
            controller.view.translatesAutoresizingMaskIntoConstraints = false
            containerView.addSubview(controller.view)
            
            NSLayoutConstraint.activate([
                controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
                controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
                controller.view.topAnchor.constraint(equalTo: containerView.topAnchor),
                controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
                ])
            
            controller.didMove(toParentViewController: self)
        
            usersViewControllers.append(controller)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
