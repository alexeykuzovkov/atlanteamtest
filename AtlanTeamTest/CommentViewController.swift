//
//  CommentViewController.swift
//  AtlanTeamTest
//
//  Created by Alex Kuzovkov on 06.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit
protocol CommentViewControllerDelegate {
    func commentViewControllerRequestsComment(viewController:CommentViewController, commentID:Int)
}
class CommentViewController: UIViewController {

    var delegate:CommentViewControllerDelegate?
    
    var maxCommentID = 10
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel!
    
    @IBOutlet var commentNumTextField: ImageTextField!
    
    @IBOutlet var loadActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var enterCommentNumberMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        clearLabels()
    }
    
    @IBAction func confirm(_ sender: Any) {
        commentNumTextField.resignFirstResponder()
        if let commentID = Int(commentNumTextField.text!) {
            var mCommentID = commentID
            if mCommentID<1 {
                mCommentID = 1
                commentNumTextField.text = "1"
            }
            if (mCommentID>maxCommentID) {
                mCommentID = maxCommentID
                commentNumTextField.text = "\(mCommentID)"
            }
            self.delegate?.commentViewControllerRequestsComment(viewController: self, commentID: mCommentID)
        }
    }
    
    func clearLabels() {
        nameLabel.text = ""
        emailLabel.text = ""
        bodyLabel.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
