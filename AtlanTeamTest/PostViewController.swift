//
//  PostViewController.swift
//  AtlanTeamTest
//
//  Created by Alex Kuzovkov on 06.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit

protocol PostViewControllerDelegate {
    func postViewControllerRequestsPost(viewController:PostViewController, postID:Int)
}

class PostViewController: UIViewController {

    var delegate:PostViewControllerDelegate?
    
    var maxPostID = 10
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel!
    @IBOutlet var postNumTextField: ImageTextField!
    @IBOutlet var loadActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var enterPostNumberMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearLabels()
    }
    @IBAction func confirm(_ sender: Any) {
        postNumTextField.resignFirstResponder()
        if let postID = Int(postNumTextField.text!) {
            var mPostID = postID
            if mPostID<1 {
                mPostID = 1
                postNumTextField.text = "1"
            }
            if (mPostID>maxPostID) {
                mPostID = maxPostID
                postNumTextField.text = "\(mPostID)"
            }
            self.delegate?.postViewControllerRequestsPost(viewController: self, postID: mPostID)
        }
    }
    
    func clearLabels() {
        titleLabel.text = ""
        bodyLabel.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
