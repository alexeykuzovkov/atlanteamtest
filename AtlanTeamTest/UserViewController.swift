//
//  UserViewController.swift
//  AtlanTeamTest
//
//  Created by Alex Kuzovkov on 06.10.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var websiteLabel: UILabel!
    @IBOutlet var adressLabel: UILabel!
    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet var companyCatchPhraseLabel: UILabel!
    @IBOutlet var companyBSLabel: UILabel!
    
    @IBOutlet var acitivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func clearLabels() {
        nameLabel.text = ""
        nicknameLabel.text = ""
        emailLabel.text = ""
        phoneLabel.text = ""
        websiteLabel.text = ""
        adressLabel.text = ""
        companyNameLabel.text = ""
        companyCatchPhraseLabel.text = ""
        companyBSLabel.text = ""

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
